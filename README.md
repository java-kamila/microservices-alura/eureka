# EUREKA (Floricultura)
Gerenciador de aplicações em estrutura de Micro-serviços.

## Estrutura

<img src="spring-micro-servicos-alura.png" alt="Estrutura dos Microservices">

 - Teremos três microsserviços: Fornecedor, Loja e Transportador

 - Uma apresentação da modelagem focado em DDD (Domain Driven Design)
     - Quebraremos o domínio em contextos menores (bounded context)
     - Um microsserviço é a implementação de um contexto

### Endpoints

<img src="endpoints_services.png" alt="Estrutura dos Microservices">

### Comunicação

<img src="eureka.png" alt="Estrutura dos Microservices">

### Anotações

 - A anotação `@EnableEurekaServer` é necessária para registrar a aplicação como um serviço Eureka
 - Pagina web em `http://localhost:8761/`
 - As configurações em `application.properties` indicam que a aplicação não é um client Eureka:
 
 ```
 eureka:
  client:
    fetch-registry: false
    register-with-eureka: false
 ```
 - O `RestTemplate` do Spring permite chamadas HTTP de alto nível

##### Requests
 	 - GET: `http://localhost:8761/eureka/apps`
 
##### Service Registry e Discovery
 - `Service registry` é um servidor central, onde todos os microsserviços ficam cadastrados (nome e IP/porta)
 - `Service discovery` é um mecanismo de descoberta do IP do microsserviço pelo nome. Dessa forma, nenhum microsserviço fica acoplado ao outro pelo IP/porta.
 
##### Spring Config Server

<img src="spring-config-server.png" alt="Servidor de COnfigurações">

 - Os microsserviços são preparados para um ambiente (cloud), cuja precificação é diretamente relacionada à quantidade de máquinas e ao uso de seus recursos de infraestrutura. Para reduzir esse custo, aplicações de microsserviços se encaixam bem, pois é possível escalar automaticamente, de acordo com a demanda, e em questão de segundos, pedaços do que antes era uma única aplicação. Nesse cenário, configurar manualmente os servidores com as configurações necessárias para cada aplicação é impraticável.
 - `@EnableConfigServer` na classe principal
 - Configurações em application.yml/properties:
 
 ```
 server:
  port: 8888
  
spring:
  profiles:
    active: native
  cloud:
    config:
      server:
        native:
          search-locations: C:/Users/Kamila/Documents/Projetos/Java/microservicos/config/microservice-repo
 ```
 - Na API **Fornecedor** é necessário configurar o acesso ao Config Server antes das demais configurações serem carregadas. Fazemos isso no arquivo `bootstrap.yml`:
 
 ```
 spring:
  application:
    name: 'fornecedor'
  profiles:
    active: default
  cloud:
    config:
      uri: http://localhost:8888
```
 
##### Balanceamento de carga - Client-Side Load-Balancing

<img src="load-balance.png" alt="Estrutura com vários fornecedores">

 - Temos três tecnologias principais nesse cenário de **Load Balancing**: O `RestTemplate`, que usamos para fazer uma requisição a uma aplicação, através do seu nome, o `Eureka Client`, que fornece as instâncias disponíveis de um determinado serviço, e o `Ribbon` que, através da anotação `@LoadBalanced` (**Loja**), aprimora o RestTemplate com o processo de Load Balancing.
 - Em Loja Server o Balanceamento de carga através de [Client-Side Load-Balancing](https://spring.io/guides/gs/spring-cloud-loadbalancer/) realiza a escolha do ip:porta do Fornecedor caso existam várias instâncias de Fornecedor na estrutura.
 - `Load Balancing` é o processo de distribuir as requisições vindas dos usuários para as várias instâncias disponíveis.
 - `Client Side Load Balancing (CSLB)` é o cliente HTTP que decide qual microsserviço recebe a requisição, podendo ter várias instâncias do mesmo serviço no ar
 - A configuração do CSLB é feita a partir da anotação @LoadBalanced, na criação do RestTemplate
 - Como implementação do CSLB, usamos um projeto chamado Ribbon, que faz parte do Spring Cloud Netflix
 - Para descobrir quais clientes existem, podemos injetar a interface `DiscoveryClient`
 - Como alternativa ao RestTemplate, podemos usar o **Spring Feign**, que já usa o Ribbon para CSLB
 - O `Spring Feign` exige apenas uma interface, com a definição e mapeamento dos métodos que executarão a requisição
 - Toda a implementação da interface é gerada pelo `Spring Feign`
	 
##### Distributed Tracing - Logg
 - Rastreamento distribuído de loggs
 - Assim como nos sistemas monolíticos, temos logs separados em máquinas diferentes, mas, apenas nos microsserviços, a lógica de negócio também está quebrada em logs diferentes
 - Uma requisição do usuário bate em várias aplicações diferentes, para que a lógica de negócio requerida seja realizada. Com isso, acompanhar os logs gerados em uma transação não é tão simples quanto abrir um único log e ter toda a informação disponível.
 - Para unificar os logs, precisamos de agregadores de log
	- Como implementação de um agregador, usamos o [PaperTrail](https://papertrailapp.com/), um agregador como serviço. Necessária criação de conta e "add system". Mantém os logs armazenados por 7 dias.
 - Usamos a biblioteca Logback para gerar e enviar os logs ao agregador
 - O Logback possui um appender, que possibilita o envio dos logs
 - Para acompanhar uma transação nos logs, usamos uma correlation-id
 - A correltation-id é um identificador da transação, que é passada de requisição pra requisição. Dessa forma, podemos entender quais requisições fazem parte da mesma transação
 - Seguindo [documentação](https://help.papertrailapp.com/kb/configuration/java-logback-logging/#syslogappender) criamos o arquivo `logback.xml` na pasta _src/main/resources_ de Fornecedor e Loja para que os logs gerados por estes systemas sejam armazenados e acessados em um único local, no PaperTrail.
 
##### Spring Sleuth
 - Bliblioteca usada para gerar a correlation-id. 
 - Seguindo [documentação](https://docs.spring.io/spring-cloud-sleuth/docs/2.2.4.RELEASE/reference/html/#json-logback-with-logstash) do [Spring Sleuth](https://docs.spring.io/spring-cloud-sleuth/docs/2.2.4.RELEASE/reference/html/) adicionada linha: `<include resource="org/springframework/boot/logging/logback/defaults.xml"/>` em logback.xml
 - Alterando `<suffixPattern>` em logback.xml para o value descrito em:
 ```xml
 <property name="CONSOLE_LOG_PATTERN"
              value="%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(${LOG_LEVEL_PATTERN:-%5p}) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n${LOG_EXCEPTION_CONVERSION_WORD:-%wEx}"/>
 ```
 
##### Circuit Breaker / Hystrix

<img src="circuit-breaker.png" />

 - Implementação de tolerância à falhas, resiliência a integrações defeituosas
 - `Circuit Breaker` tem como funcionalidade principal a análise das requisições anteriores, para decidir se deve parar de repassar as requisições vindas do cliente para um microsserviço com problemas de performance. Enquanto o circuito está fechado, o Hystrix continua tentando a cada 5 segundos, com o objetivo de verificar se o servidor voltou a funcionar normalmente.
 - O `Circuit Breaker` implementado pelo `Hystrix` executa o processamento em uma thread separada. Quando o tempo limite é excedido, o Hystrix mata a execução da thread e, caso configurado, repassa a execução para o método de Fallback, de forma que este possa implementar livremente um tratamento de erro.
 - O método `Fallback` trata a interrupção da Thread efetuada pelo Circuit Breaker.
 
##### Bulkhead com Hystrix

<img src="bulkhead.png" />
 
 - Objetiva não indisponibilizar toda a aplicação por causa de uma única funcionalidade. 
 - `Bulkhead Pattern` - Implementação para poder agrupar e alocar grupos de threads para processamentos diferentes. Dessa forma, uma das chamadas de um microsserviço, que sofre lentidão por causa de uma integração com problema de performance, não indisponibiliza todas as outras chamadas do mesmo microsserviço
 - o `Hystrix` executa uma gerência de um pool de threads
 - A combinação de um volume alto de requisições para um único serviço de um microsserviço pode indisponibilizar as outras requisições
 
##### Tratando Erros

<img src="tratando-erros.png" />
 
 - Loja Server controla o status da compra e retorna para o cliente
 - A cada nova transação o status da compra é atualizado e persistido em banco de dados
 - Caso aconteça alguma exceção o método `Fallback` é chamado
 
##### API Gateway

<img src="zuul.png" />

 - Uma `API Gateway` é útil para que os usuários consigam acessar as funcionalidades implementadas em vários microsserviços, sem que os usuários (aplicativo móvel, web, etc) tenham a inteligência de saber como encontrar os microsserviços.
 - Não é recomendado expor o server `Eureka` na Internet para que os front-ends descubram as instâncias disponíveis.
 
##### API Autenticação

<img src="autenticacao.png" />

 - Quando o usuário faz uma requisição para um serviço que demanda que o usuário esteja logado, no header da requisição, o usuário envia um token, que ele conseguiu previamente através do servidor de autenticação.
 - A integração do **Spring Security** e o **Spring Cloud OAuth** é feita quanto estendemos o WebSecurityConfigurerAdapter e expomos os recursos do Spring Security para serem utilizados pelo Spring Cloud OAuth.
 - `AuthenticationManager` e `UserDetailService` são os beans expostos do `Spring Security` e injetados no Adapter do Spring Cloud OAuth2: o `AuthorizationServerConfigurerAdapter`. Mais especificamente, a integração é feita no método configure deste adapter.


##### Autenticação em um Microservice

 - Para habilitar a autenticação do usuário foi adicionada a anotation `@EnableResourceServer` em *LojaApplication* e criada a classe `ResourceServerConfigurer`que estende de `ResourceServerConfigurerAdapter`, onde são sinalizados os recursos e e configurações de autenticação.
 - A aplicação passa a buscar informações do client que está tentando acessar os recursos na API Authentication através do url indicado em `application.properties`:
 `security.oauth2.resource.user-info-uri=http://localhost:8088/user`
 
###### Repassando token entre Microservices (Feign interceptor)

 - Inserir configuração de segurança do hystrix em application.properties para habilitar captura de informações de segurança da thread que acessou o recurso.
 `hystrix.shareSecurityContext: true` 
 - Inserir na classe principal a interceptação das requsições para captura do token:
 ```java
 	@Bean
	public RequestInterceptor getInterceptorAutenticacao() {
		return new RequestInterceptor() {
			
			@Override
			public void apply(RequestTemplate template) {
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				if (authentication == null) {
					return;
				}
				
				OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails)authentication.getDetails();
				template.header("Authorization", "Bearer" + details.getTokenValue());
			}
		};
	}
```
 